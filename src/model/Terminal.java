package model;

import model.logger.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Terminal extends JFrame{
	private static Terminal instance;

	private JPanel consolePanel;
	private JTextArea commandTA;
	private JScrollPane jsp;
	
	private List<ObserverESEO> observers;
	
	/**
	 * Builds a new Terminal frame
	 */
	private Terminal(){
		createControls();
		this.add(consolePanel);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		observers = new ArrayList<ObserverESEO>();
	}

	/**
	 * Initializes all controls for the TerminalFrame
	 */
	private void createControls() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.consolePanel = new JPanel();
		this.consolePanel.setBackground(Color.black);
		createTextArea();
		this.consolePanel.add(jsp);
		
	}
	
	/**
	 * Initializes the terminal text area
	 */
	private void createTextArea() {
		this.commandTA = new JTextArea(24,80);
		this.commandTA.setBackground(Color.black);
		this.commandTA.setForeground(Color.green);
		this.commandTA.setCaretColor(this.commandTA.getForeground());
		this.commandTA.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent event){
				Terminal.this.notify(String.valueOf(event.getKeyChar()));
			}
		});;
		this.jsp = new JScrollPane(commandTA);
	}
	
	public static Terminal getInstance(){
		if(instance==null){
			instance = new Terminal();
		}
		return instance;
	}
	
	public Logger addLogger(Logger logger){
		addObserverESEO(logger);
		return logger;
	}
	
	public void removeLogger(String loggerName){
		ObserverESEO obs = null;
		for (ObserverESEO observerESEO : observers) {
			if(observerESEO.getClass().getSimpleName().equals(loggerName)){
				obs = observerESEO;
			}
		}
		removeObserverESEO(obs);
	}

	public void addObserverESEO(ObserverESEO obs){
		observers.add(obs);
	}
	
	public void removeObserverESEO(ObserverESEO obs){
		observers.remove(obs);
	}
	
	/**
	 * Notifies all observers when a change occurs
	 * @param text the content of the terminal to synchronize with all observers
	 */
	public void notify(String text){
		for(ObserverESEO obs : this.observers){
			obs.update(text);
		}
	}
}

