package model.strategy;

/**
 * @author Tristan LE GACQUE
 * Created 26/11/2018
 */
public class RawStrategy implements MessageBuildingStrategy {
    @Override
    public String buildLogMessage(String logEntry) {
        return logEntry;
    }

    @Override
    public String toString() {
        return "Raw";
    }
}
