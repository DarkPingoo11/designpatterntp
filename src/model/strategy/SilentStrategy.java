package model.strategy;

/**
 * @author Tristan LE GACQUE
 * Created 26/11/2018
 */
public class SilentStrategy implements MessageBuildingStrategy {

    @Override
    public String buildLogMessage(String logEntry) {
        return logEntry.replaceAll(".", "*");
    }

    @Override
    public String toString() {
        return "Silent";
    }
}
