package model.strategy;

public interface MessageBuildingStrategy {
	
	public String buildLogMessage(String logEntry);

}
