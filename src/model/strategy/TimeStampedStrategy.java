package model.strategy;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tristan LE GACQUE
 * Created 26/11/2018
 */
public class TimeStampedStrategy implements MessageBuildingStrategy {
    @Override
    public String buildLogMessage(String logEntry) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return "[" + format.format(new Date()) + "] " + logEntry + "\n";
    }

    @Override
    public String toString() {
        return "Timestamp";
    }
}
