package model.logger;

/**
 * Logs contents of the Terminal to standard output
 */
public class ConsoleLogger extends Logger {
	
	@Override
	public void update(String text) {
		log(text);
	}
	
	@Override
	public void log(String message){
		System.out.print(this.getMessageBuilder().buildLogMessage(message));
	}

}
