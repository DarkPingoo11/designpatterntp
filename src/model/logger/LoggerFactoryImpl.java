package model.logger;
public class LoggerFactoryImpl extends LoggerFactory {

	@Override
	public Logger creeLogger(String className) {
		switch(className){
			case "ConsoleLogger": return creeConsoleLogger();
			case "LogFrame": return creeFrameLogger();
			case "FileLogger": return creeFileLogger();
			default:
				throw new IllegalArgumentException("La classe '" + className + "' n'est pas supportée.");
		}
	}

	@Override
	public Logger creeConsoleLogger() {
		ConsoleLogger consoleLogger = new ConsoleLogger();
		return consoleLogger;
	}

	@Override
	public Logger creeFrameLogger() {
		LogFrame frameLogger = new LogFrame();
		frameLogger.getLogFrame().setVisible(true);
		return frameLogger;
	}

	@Override
	public Logger creeFileLogger() {
		FileLogger fileLogger = new FileLogger();
		return fileLogger;
	}
}
