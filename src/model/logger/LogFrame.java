package model.logger;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;

/**
 * Simple JFrame with JTextArea that should copy content of the Terminal
 *
 */
public class LogFrame extends Logger{
	private JFrame logFrame;
	private JTextArea textArea;
	
	public LogFrame(){
		logFrame = new JFrame();
		textArea = new JTextArea();
		textArea.setPreferredSize(new Dimension(500,500));
		logFrame.getContentPane().add(textArea);
		logFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		logFrame.pack();
	}

	public JFrame getLogFrame() {
		return logFrame;
	}
	
	/**
	 * Appends text to the contents of the text area
	 * @param text the text to log on the frame
	 */
	public void writeText(String text){
		textArea.append(text);
	}
	
	/**
	 * Removes last character of the text area
	 */
	public void removeLast(){
		try {
			textArea.setText(textArea.getText(0, textArea.getText().length()-1));
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(String text) {
		log(text);
	}
	
	@Override
	public void log(String message){
		if(message.codePointAt(0) != 8){ // Backspace
			writeText(this.getMessageBuilder().buildLogMessage(message));
		} else {
			removeLast();
		}
	}
}
