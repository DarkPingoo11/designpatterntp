package model.logger;

import model.ObserverESEO;
import model.strategy.MessageBuildingStrategy;
import model.strategy.SilentStrategy;

public abstract class Logger implements ObserverESEO {
	public MessageBuildingStrategy strategy;
	
	public Logger(){
		this.strategy = new SilentStrategy();
	}
	
	public abstract void log(String message);
	
	public void setStrategy(MessageBuildingStrategy loggingStrategy) {
		strategy = loggingStrategy;
	}

	public MessageBuildingStrategy getMessageBuilder() {
	    return this.strategy;
    }
}
