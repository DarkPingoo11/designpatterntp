package model.logger;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Calendar;

/**
 * Logs contents of the Terminal to a file
 */
public class FileLogger extends Logger{
	
	private File logFile;
	
	/**
	 * Constructor -> appends current date to the log file
	 */
	public FileLogger(){
		logFile = new File("text.log");
		Calendar cal = Calendar.getInstance(); 
		cal.setTimeInMillis(System.currentTimeMillis());
		log("\n\nLog file - "+cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.YEAR)+"\n");
	}
	
	@Override
	public void log(String text){
		try {
			if(!logFile.exists()){
				logFile.createNewFile();
			}
			Files.write(logFile.toPath(), this.getMessageBuilder().buildLogMessage(text).getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(String text) {
		log(text);
	}
}
