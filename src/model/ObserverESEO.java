package model;
/**
 * ObserverESEO provided an interface for implementation to
 * update data (especially text) provided by a specific terminal.
 */
public interface ObserverESEO {
	/**
	 * Synchronizes data among terminal and loggers
	 * @param text received from terminal
	 */
	public void update(String text);
}
