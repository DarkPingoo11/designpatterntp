package model;

import config.ConfigFrame;
import model.strategy.MessageBuildingStrategy;
import model.strategy.RawStrategy;
import model.strategy.SilentStrategy;
import model.strategy.TimeStampedStrategy;

/**
 * Main class for illustrating observers in action
 */
public class RecordActivity {

	public static void main(String[] args) {
		Terminal term = Terminal.getInstance();
		term.setVisible(true);

		MessageBuildingStrategy[] strategies = new MessageBuildingStrategy[3];
		strategies[0] = new SilentStrategy();
		strategies[1] = new TimeStampedStrategy();
		strategies[2] = new RawStrategy();

		ConfigFrame cf = new ConfigFrame(strategies);
		cf.setVisible(true);
	}
}
