package command;

import model.Terminal;
import model.logger.Logger;
import model.logger.LoggerFactory;
import model.logger.LoggerFactoryImpl;

public class AddLoggerCommand extends Command {

	private String loggerName;
	private Logger logger;

	public AddLoggerCommand(String loggerName) {
		this.loggerName = loggerName;
	}

	@Override
	public Logger execute() {
		if(this.logger == null) {
			LoggerFactory loggerFactory = new LoggerFactoryImpl();
			this.logger = loggerFactory.creeLogger(this.loggerName);
		}

		System.out.println("Ajout du logger " + this.loggerName);

		Terminal.getInstance().addLogger(logger);
		return logger;
	}

}
