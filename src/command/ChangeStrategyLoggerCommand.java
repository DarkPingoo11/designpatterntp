package command;

import model.logger.Logger;
import model.strategy.MessageBuildingStrategy;

/**
 * @author Tristan LE GACQUE
 * Created 26/11/2018
 */
public class ChangeStrategyLoggerCommand extends Command {

    private Logger logger;
    private MessageBuildingStrategy strategy;

    public ChangeStrategyLoggerCommand(Logger logger, MessageBuildingStrategy strategy) {
        this.logger = logger;
        this.strategy = strategy;
    }

    @Override
    public Logger execute() {
        System.out.println("Changement de la strategie de " + logger.getClass().getSimpleName() + " : " + strategy);

        this.logger.setStrategy(strategy);
        return this.logger;
    }

}
