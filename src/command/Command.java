package command;

import model.logger.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;

public abstract class Command extends AbstractAction{
	
	public abstract Logger execute();
	
	@Override
	public void actionPerformed(ActionEvent e) {
		execute();
	}
}
