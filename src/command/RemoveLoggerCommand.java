package command;

import model.Terminal;
import model.logger.Logger;

public class RemoveLoggerCommand extends Command {

	private String loggerName;

	public RemoveLoggerCommand(String loggerName) {
		this.loggerName = loggerName;
	}

	@Override
	public Logger execute() {
		System.out.println("Retrait du logger " + this.loggerName);

		Terminal.getInstance().removeLogger(this.loggerName);
		return null;
	}

}
