package config;

import command.AddLoggerCommand;
import command.ChangeStrategyLoggerCommand;
import command.Command;
import command.RemoveLoggerCommand;
import model.Terminal;
import model.logger.Logger;
import model.strategy.MessageBuildingStrategy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ConfigFrame extends JFrame {
	
	JLabel frameLabel;
	JLabel fileLabel;
	JLabel consoleLabel;
	
	JCheckBox addFileLogger;
	JCheckBox addFrameLogger;
	JCheckBox addConsoleLogger;

	Logger fileLogger;
	Logger consoleLogger;
	Logger frameLogger;
	
	JComboBox<MessageBuildingStrategy> selectStrategyFrame;
	JComboBox<MessageBuildingStrategy> selectStrategyFile;
	JComboBox<MessageBuildingStrategy> selectStrategyConsole;

    private Command cmdAddFL = new AddLoggerCommand("FileLogger");
    private Command cmdAddLF = new AddLoggerCommand("LogFrame");
    private Command cmdAddCL = new AddLoggerCommand("ConsoleLogger");
    private Command cmdRemoveFL = new RemoveLoggerCommand("FileLogger");
    private Command cmdRemoveLF = new RemoveLoggerCommand("LogFrame");
    private Command cmdRemoveCL = new RemoveLoggerCommand("ConsoleLogger");
    
	
	public ConfigFrame(MessageBuildingStrategy[] strategies){
		createControls(strategies);
		createListeners();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createControls(MessageBuildingStrategy[] strategies){
		selectStrategyFrame = new JComboBox<>(strategies);
		selectStrategyFile = new JComboBox<>(strategies);
		selectStrategyConsole = new JComboBox<>(strategies);
		selectStrategyFrame.setEnabled(false);
		selectStrategyFile.setEnabled(false);
		selectStrategyConsole.setEnabled(false);
		
		fileLabel = new JLabel("FileLogger");
		fileLabel.setPreferredSize(new Dimension(200,(int)fileLabel.getPreferredSize().getHeight()));
		frameLabel = new JLabel("LogFrame");
		frameLabel.setPreferredSize(new Dimension(200,(int)frameLabel.getPreferredSize().getHeight()));
		consoleLabel = new JLabel("ConsoleLogger");
		consoleLabel.setPreferredSize(new Dimension(200,(int)consoleLabel.getPreferredSize().getHeight()));
		
		addFileLogger = new JCheckBox();
		addFileLogger.setActionCommand("FileLogger");
		addFrameLogger = new JCheckBox();
		addFrameLogger.setActionCommand("LogFrame");
		addConsoleLogger = new JCheckBox();
		addConsoleLogger.setActionCommand("ConsoleLogger");
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.add(newConfigurationEntry(fileLabel, addFileLogger, selectStrategyFile));
		mainPanel.add(newConfigurationEntry(frameLabel, addFrameLogger, selectStrategyFrame));
		mainPanel.add(newConfigurationEntry(consoleLabel, addConsoleLogger, selectStrategyConsole));
		this.setTitle("Configuration Logger");
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		this.setLocation(Terminal.getInstance().getLocation().x,Terminal.getInstance().getLocation().y-200);
		this.pack();
	}

	private Box newConfigurationEntry(JLabel label, JCheckBox checkBox, JComboBox comboBox) {
		Box box = Box.createHorizontalBox();
		box.add(Box.createHorizontalStrut(50));
		box.add(label);
		box.add(checkBox);
		box.add(Box.createHorizontalStrut(5));
		box.add(comboBox);
		return box;
	}
	
	private void createListeners() {
		selectStrategyFrame.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(addFrameLogger.isSelected() && e.getStateChange()== ItemEvent.SELECTED) {
                    MessageBuildingStrategy strategy = (MessageBuildingStrategy) e.getItem();
                    new ChangeStrategyLoggerCommand(frameLogger, strategy).execute();
				}
			}
		});
		selectStrategyFile.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(addFileLogger.isSelected() && e.getStateChange()== ItemEvent.SELECTED) {
                    MessageBuildingStrategy strategy = (MessageBuildingStrategy) e.getItem();
                    new ChangeStrategyLoggerCommand(fileLogger, strategy).execute();
				}
			}
			
		});
		selectStrategyConsole.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(addConsoleLogger.isSelected() && e.getStateChange()== ItemEvent.SELECTED) {
				    MessageBuildingStrategy strategy = (MessageBuildingStrategy) e.getItem();
                    new ChangeStrategyLoggerCommand(consoleLogger, strategy).execute();
				}
			}
			
		});


		addFileLogger.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if(cb.isSelected()){
                    fileLogger = cmdAddFL.execute();
					selectStrategyFile.setEnabled(true);
				} else {
                    cmdRemoveFL.execute();
					selectStrategyFile.setEnabled(false);
				}
			}
		});
		addFrameLogger.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if(cb.isSelected()){
                    frameLogger = cmdAddLF.execute();
					selectStrategyFrame.setEnabled(true);
				} else {
                    cmdRemoveLF.execute();
					selectStrategyFrame.setEnabled(false);
				}
			}
		});
		addConsoleLogger.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if(cb.isSelected()){
					consoleLogger = cmdAddCL.execute();
					selectStrategyConsole.setEnabled(true);
				} else {
                    cmdRemoveCL.execute();
					selectStrategyConsole.setEnabled(false);
				}
			}
		});
	}
}
